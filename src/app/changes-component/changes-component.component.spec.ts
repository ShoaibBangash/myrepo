import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangesComponentComponent } from './changes-component.component';

describe('ChangesComponentComponent', () => {
  let component: ChangesComponentComponent;
  let fixture: ComponentFixture<ChangesComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangesComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
