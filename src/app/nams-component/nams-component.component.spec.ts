import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NamsComponentComponent } from './nams-component.component';

describe('NamsComponentComponent', () => {
  let component: NamsComponentComponent;
  let fixture: ComponentFixture<NamsComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NamsComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NamsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
