import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NamsComponentComponent } from './nams-component/nams-component.component';
import { ChangesComponentComponent } from './changes-component/changes-component.component';

@NgModule({
  declarations: [
    AppComponent,
    NamsComponentComponent,
    ChangesComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
